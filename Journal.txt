OCT 29
Things I have learned during a portion of the first assignment:
- Familiarizing myself with php syntax.
- Familiarizing myself with the Laravel framework, its structure and how the different pages are linked together.
- How to use terminal commands to generate an address and run the the code on the browser.
- How to make use of variables, arrays, and loops with php.


OCT 30
I decided to replicate the website Medium. It has a simple design and it has a content feed.
A big portion of this assignment was a refresher on html and css. This was a good exercise to practice more of styling and writing content. I learned how to use php code and route data from a different page in the Laravel framework.
I was having some difficulties getting the styles of the layout of the featured articles section just right. I used flex box but some of the margins of the content is not exactly lining up how I intended it to.
I am not quite sure how to use controllers so I did not do that part of the assignment.
I could improve on the style sheet to get rid of some repeating styles, make less lines of code, and have more consistency, but ran out of time.


Nov 1
I missed a  day of class this week, due to being sick, when the topic of databases was being introduced. This was a big hurdle to overcome as the material being covered is moving pretty fast. I relied a lot on the screen casts that Cameron had provided, this was a very helpful tool in order to get up to speed.
Some important lessons I have learned so far about the material:
An object is an instance of a class.
A method is a function that is attached to a class.
The general flow within the Laravel framework is routes to controllers to views.
Using @extends we can inherit all the properties and methods of a parent class but then override certain ones if needed.
Refactoring is changing the code without changing the functionality.


Nov 5
Since missing a day I have not been able to follow along with coding in class. So my strategy has been to just listen in class while the material is being presented and then follow along with coding from the youtube videos, and ask questions if necessary. It has worked alright to a certain extent and my twitter page is functional and getting data from a database, but I am still behind and on the topic of migrations at the moment.


Nov 6
The final assignment is due tonight.
Since the first assignment I have:
Added documentation to the top of the code that I wrote or edited, such as the author and date.
Fixed some of the indentation for better code style and readability.
Created a Controller page with the necessary methods.
Created a layout page to better separate the views of the website.
Created a model class called Article to allow for connection to the database.
I created a MySQL table with columns corresponding to the properties I had made for the articles.
I updated the .env file and created a demo page to test out the connection to the database. This was successful, the demo page demonstrates the database that I created.
When Trying to update the the controller page in order to get the article information from the database I was getting some errors which I wasn't able to debug. Something about properties not existing on the collection instance.

The site is deployed to AWS EC2
http://35.183.203.145/
