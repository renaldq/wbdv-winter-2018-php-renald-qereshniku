<pre>
<?php

$a = 0;

function doSomething($a) {
    echo $a;
}

function returnSomething($a) {
    return $a;
}

doSomething($a);

echo $a;
