<pre>
<?php


// $bob = [
//     'name' => 'Bob',
//     'age' => 21
// ];

// $bobObject = (object)$bob;
// var_dump($bobObject);

// echo "\n";

class Cake {
    public $type;
    private $secretIngredient;

    public function __construct($type, $secretIngredient)
    {
        $this->type = $type;
        $this->secretIngredient = $secretIngredient;
    }

    public function describeTaste() {
        return 'Yummy '.$this->type.'!';
    }

    public function recipe() {
        return $this->secretIngredient;
    }
}

$cake = new Cake();
echo $cake->describeTaste().'<br/>';
echo $cake->recipe();