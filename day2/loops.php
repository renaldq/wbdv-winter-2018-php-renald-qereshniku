<pre>
<?php

$list = [
    'a', 'b', 'c'
];

// preffered way: foreach loop
foreach ($list as $value) {
    echo $value."\n";
}

// old school way of loops
for ($i=0; $i < count($list); $i++) {
    echo $list[$i]."\n";
}

$bob = [
    'name' => 'Bob',
    'age' => 21
]
$jane = [
    'name' => 'Jane',
    'age' => 24
]

$people = [$bob, $jane];

echo "\n";

foreach ($people as $value) {
    foreach ($value as $k => $v) {
        echo "$k: $v\n";
    }
}