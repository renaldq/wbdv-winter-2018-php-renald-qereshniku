<?php

// author: Renald Qereshniku
// date: Nov 6,2018

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\Article;

class Article {}

class ArticlesController extends Controller
{

    public function demo()
    {
        $articles = Article::all();

        echo '<pre>';
        var_dump( $articles );
    }

    public function index()
    {

        // $featured = $this->getArticles();

        $featured1 = new Article();
        $featured1->photo = 'https://cdn-images-1.medium.com/fit/c/416/151/1*3Pa-bW9tZoKuLODrUwXIHw.jpeg';
        $featured1->title = 'How Power Poses Took Over the World';
        $featured1->preview = 'Psychologists continue to grapple over whether standing like Superman changes the mind—and the body';
        $featured1->author = 'Dalmeet Singh Chawla';
        $featured1->category = 'Power Trip';
        $featured1->posted = 'yesterday';
    
        $featured2 = new Article();
        $featured2->photo = 'https://cdn-images-1.medium.com/focal/100/100/50/61/1*Js7LSUIe5b65-8uA64uSTA.jpeg';
        $featured2->title = 'Facebook Is the New Climate Change';
        $featured2->preview = 'A little imagination might be the key to...';
        $featured2->author = 'Colin Horgan';
        $featured2->category = '';
        $featured2->posted = 'Oct 27';
    
        $featured3 = new Article();
        $featured3->photo = 'https://cdn-images-1.medium.com/focal/100/100/52/46/1*5t80Zzs6p1PAJZNOiMy16A.jpeg';
        $featured3->title = 'Have We Got a Cure for You!';
        $featured3->preview = 'Navigating the pharmaceutical industry...';
        $featured3->author = 'Laura Entis';
        $featured3->category = 'Power Trip';
        $featured3->posted = 'Oct 30';
    
        $featured4 = new Article();
        $featured4->photo = 'https://cdn-images-1.medium.com/fit/c/100/100/1*gEJ-6EZHw9ajCsqJ0ytmqw.jpeg';
        $featured4->title = 'When Big Soda Started Stalking Me';
        $featured4->preview = 'An unlikely gift from Russian hackers';
        $featured4->author = 'Marion Nestle';
        $featured4->category = 'Power Trip';
        $featured4->posted = 'Oct 30';
    
        $featured5 = new Article();
        $featured5->photo = 'https://cdn-images-1.medium.com/focal/504/222/44/62/1*KjT3oOEwDh6ULaFgfrHMHg.jpeg';
        $featured5->title = 'The Overwhelming Hugeness of Climate Change';
        $featured5->preview = 'Ditching fossil fuels will take a lot more...';
        $featured5->author = 'Casey Williams';
        $featured5->category = '';
        $featured5->posted = 'Oct 29';
    
        $viewData = [
            'featuredArticlesLeft' => $featured1,
            'featuredArticlesMiddle' => [
                $featured2,
                $featured3,
                $featured4,
            ],
            'featuredArticlesRight' => $featured5,
        ];
    
        return view('welcome', $viewData);
    }

    public function getArticles()
    {
        $articles = Article::all();

        return $articles;
    }
}
