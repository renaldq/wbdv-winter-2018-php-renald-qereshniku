<!-- author: Renald Qereshniku -->
<!-- date: Nov 6,2018 -->

<!DOCTYPE html>

<html>

  <head>
    <meta charset="utf-8">
    <title>Medium – a place to read and write big ideas and important stories</title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/app.css">
  </head>

  <body class="wrapper">

    <div class="top-container">
      <div class="logo">
        Medium
      </div>
      <div class="top-right-container">
        <i class="fas fa-search"></i>
        <div class="member">Become a member</div>
        <div class="sign-in green">Sign in</div>
        <button>Get Started</button>
      </div>
    </div>

    <ul class="nav-links">
      <li><a href="#">Home</a></li>
      <li><a href="#">Power Trip</a></li>
      <li><a href="#">Culture</a></li>
      <li><a href="#">Tech</a></li>
      <li><a href="#">Startups</a></li>
      <li><a href="#">Self</a></li>
      <li><a href="#">Politics</a></li>
      <li><a href="#">Design</a></li>
      <li><a href="#">Health</a></li>
      <li><a href="#">Popular</a></li>
      <li><a href="#">Collections</a></li>
      <li><a href="#">More</a></li>
    </ul>

    <div class="top-stories flex flex-h">
      @yield('featured')
    </div>

    <div class="hero">
      @yield('hero')
    </div>

  </body>

</html>