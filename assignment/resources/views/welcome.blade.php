<!-- author: Renald Qereshniku -->
<!-- date: Nov 6,2018 -->

@extends('layout')

@section('featured')

  <div class="column-one flex-v flex-1">
    <div class="photo">
      <img src="<?php echo $featuredArticlesLeft->photo ?>">
    </div>
    <div class="title">
      <?php echo $featuredArticlesLeft->title ?> <br>
    </div>
    <div class="preview">
      <?php echo $featuredArticlesLeft->preview ?> <br>
    </div>
    <div class="author-and-category">
      <?php echo $featuredArticlesLeft->author ?> in 
      <?php echo $featuredArticlesLeft->category ?> <br>
    </div>
    <div class="posted">
      <?php echo $featuredArticlesLeft->posted ?> <br>
    </div>
  </div>

    <div class="column-two flex-1">
    <?php foreach ($featuredArticlesMiddle as $article): ?>
        <div class="flex">
        <div class="">
            <img src="<?php echo $article->photo ?>"
            style="margin-right: 24px; margin-bottom: 24px;"
            >
        </div>
        <div class="flex-1">
            <div class="fw-b">
            <?php echo $article->title ?> <br>
            </div>
            <div class="font-opensans grey">
            <?php echo $article->preview ?> <br>
            </div>
            <?php echo $article->author ?> in <?php echo $article->author ?> <br>
            <?php echo $article->posted ?> <br>
        </div>
        </div>
    <?php endforeach; ?>
    </div>
    <div class="column-three flex-v flex-1">
    <div class="photo">
        <img src="<?php echo $featuredArticlesRight->photo ?>">
    </div>
    <div class="title">
        <?php echo $featuredArticlesRight->title ?> <br>
    </div>
    <div class="preview grey">
        <?php echo $featuredArticlesRight->preview ?> <br>
    </div>
    <div class="author-and-category">
        <?php echo $featuredArticlesRight->author ?> in 
        <?php echo $featuredArticlesRight->category ?> <br>
    </div>
    <div class="posted">
        <?php echo $featuredArticlesRight->posted ?> <br>
    </div>
</div>
@endsection


@section('hero')
<div class="hero-header1">
  Welcome to Medium,<br>where words matter.
</div>
<div class="hero-header2">
  We’ll deliver the best stories and ideas on the topics you <br>
  care about most straight to your homepage, app, or inbox.
</div>
@endsection