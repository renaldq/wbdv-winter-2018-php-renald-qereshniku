<?php

include_once './Shop/Cake.php';
include_once './Shop/Oven.php';
include_once './Shop/Staff.php';
include_once './School/Student.php';

use School\Student as SchoolStudent;
use Shop\Student as ShopStudent;
use Schoool\Building\Ict\Classroom as Classroom;

function main() {
    $cake = new Cake();
    $bob = new Staff();
    echo $bob->eat($cake);

    $oven = new Oven();

    $jill = new Student();
    $classroom = new Classroom();
}

main();