@extends('layout')

@section('content')
<div class="user-details flex-1 p-10">
  <div class="fz-4 fw-bold mb-0 pb-0 lh-1">
    <?php echo $user->name ?>
  </div>
  <div class="c-3">
    <?php echo $user->handle ?>
  </div>
  <div class="my-3">
    <?php echo $user->description ?>
  </div>
  <div class="c-3">
    <i class="fas fa-link"></i> <a href=""><?php echo $user->website ?></a>
  </div>
  <div class="c-3">
    Joined <?php echo $user->joined->format('M Y') ?>
  </div>
</div>

<div class="tweets flex-2 bg-white p-10">
  <?php foreach ($tweets as $tweet): ?>
    @include ('tweet')
  <?php endforeach; ?>
</div>

<div class="suggestion flex-1 p-10">
  <div class="fz-3 fw-bold">
    You may also like
  </div>
  <?php foreach ($youMightLike as $tweeter): ?>
    <div class="flex mb-2">
      <div class="mr-2">
        <img src="<?php echo $tweeter->image ?>"
          alt=""
          class="circular"
          style="width: 50px"
          >
      </div>
      <div class="flex-1">
        <span class="fw-bold"><?php echo $tweeter->name ?></span><br>
        <?php echo $tweeter->handle ?>
      </div>
    </div>
  <?php endforeach; ?>
</div>
@endsection

@section('headerBottom')
<div class="stats bg-white py-10">
  <div class="flex flex-h flex-center">
    <div class="stat px-10">
      <div class="label fw-bold">
        Tweets
      </div>
      <div class="value">
      <?php echo $user->tweets ?>
      </div>
    </div>
    <div class="stat px-10">
      <div class="label fw-bold">
        Following
      </div>
      <div class="value">
        <?php echo $user->following ?>
      </div>
    </div>
    <div class="stat px-10">
      <div class="label fw-bold">
        Followers
      </div>
      <div class="value">
        <?php echo $user->followers ?>
      </div>
    </div>
    <div class="stat px-10">
      <div class="label fw-bold">
        Likes
      </div>
      <div class="value">
        <?php echo $user->likes ?>
      </div>
    </div>
    <div class="stat px-10">
      <div class="label fw-bold">
        Lists
      </div>
      <div class="value">
        <?php echo $user->lists ?>
      </div>
    </div>
  </div>
</div>
@endsection