<div class="flex mb-3">
  <div class="mr-2">
    <img
      src="<?php echo $tweet->user->image ?>"
      alt=""
      class="circular"
      style="width: 50px"
    >
  </div>
  <div class="">
    <span class="fw-bold"><?php echo $tweet->user->name ?></span>
    <?php echo $tweet->user->handle ?>
    <?php echo $tweet->date->format('M j') ?>
    <div class="">
      <?php echo $tweet->content ?>
    </div>
  </div>
</div>