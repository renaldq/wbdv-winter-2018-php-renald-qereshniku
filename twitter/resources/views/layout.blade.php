<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body>
    <header class="flex flex-v">
      <div class="top-top bg-white p-10">
        Top Top
      </div>
      <div class="hero-row bg-2">
        <!-- <img src="https://images.unsplash.com/photo-1522124624696-7ea32eb9592c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=decc4d1643fb3710366aa1346573ffc8&auto=format&fit=crop&w=1950&q=80" width="100%"> -->
      </div>
      @yield('headerBottom')
    </header>
    <main class="flex full-page">
        @yield('content')
    </main>
  </body>
</html>