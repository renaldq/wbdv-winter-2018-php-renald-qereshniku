<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'TweetsController@index');
Route::get('/demo', 'TweetsController@demo');
Route::get('/contacts', 'ContactsController@index');
Route::get('/{id}', 'TweetsController@index');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
