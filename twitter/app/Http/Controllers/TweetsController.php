<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;
use App\Models\Tweet;
use App\Models\User;


class TweetsController extends Controller
{

    public function demo()
    {

        $tweets = Tweet::all();

        echo '<pre>';
        var_dump( $tweets );
        return '';
    }

    public function index($id = 1)
    {
        $primaryUser = $this->getPrimaryUser($id);
        $youMightLikeUsers = $this->getYouMightLikeUsers($primaryUser);
        $tweets = $this->getTweets($primaryUser);

        $viewData = [
            'user' => $primaryUser,
            'youMightLike' => $youMightLikeUsers,
            'tweets' => $tweets
        ];

        return view('welcome', $viewData);
    }
 
    public function getPrimaryUser($id)
    {
        $primaryUser = User::findOrFail($id);
 
        $primaryUser->tweets = '5,917';
        $primaryUser->following = '43';
        $primaryUser->followers = '13M';
        $primaryUser->likes = '1';
        $primaryUser->lists = '9';
   
        return $primaryUser;
    }

    public function getYouMightLikeUsers($primaryUser)
    {
        return User::where('id', '!=', $primaryUser->id)->get();
    }

    public function getTweets($primaryUser)
    {
        $tweets = Tweet::where('user_id', $primaryUser->id)->get();

        return $tweets;
    }
}
